
package com.eng.number.controller;

import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class FibunacciController {
    private static final Logger log = LoggerFactory.getLogger(FibunacciController.class);

    
    /**
     * 
     * @param n
     * @return a list of fibonaci numbers which are less than or equal to n.
     */
    	@RequestMapping(path = "/fibonacci/{n}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<Integer> getLookupTableAll(@PathVariable int n) {
        log.info("Get all fibonacci numbers under n");
        return Collections.emptyList();
    }
}
