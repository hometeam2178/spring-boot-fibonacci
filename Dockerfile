FROM centos:7
RUN mkdir -p /apps/lookuptypecoreservice/ && chmod 755 /apps/lookuptypecoreservice/
ADD target/ngp-LookupTypeCoreService-0.0.1-SNAPSHOT.jar /apps/lookuptypecoreservice/
WORKDIR /apps/lookuptypecoreservice/
RUN yum install -y java-1.8.0-openjdk
ENV JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64/jre
ENV PATH=$PATH:$JAVA_HOME/bin
#EXPOSE 8085
ENTRYPOINT ["java", "-jar", "LookupTypeCoreService-0.0.1-SNAPSHOT.jar", "&" ]



