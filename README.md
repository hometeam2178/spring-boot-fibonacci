Interview Questions:

1. Implement a restful service that gets all of the fib numbers no greater than N.
2. Implement basic input validations
3. Implement exception handling assuming a possible DB exception.
4. Implement an in memory cache for the fib numbers. what will be the key and value in the cache?

